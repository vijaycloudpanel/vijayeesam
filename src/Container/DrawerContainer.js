import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({

    containerStyle: {
        flex: 1,
        backgroundColor: '#4C144C'
    },

    leftTextStyle: {
        fontSize: 16,
        marginLeft: 20,
        color: '#fff',

    },
    

})