import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    headerStyle: { backgroundColor: "white", height: 55, elevation: 5 },
    headerText: { fontSize: 16, color: "#000", width: 200, },
    offLineText: {alignSelf: 'center', marginTop: '40%' },
    cardView: { margin: 10, backgroundColor: '#fff', borderWidth: 0.5, borderColor: '#f5f5f5', elevation: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderRadius: 5 },
    ImageStyle: { height: 65, width: 65, borderRadius: 50, marginLeft: 10, marginTop: 5, marginBottom: 5 },
    ButtonStyle: { alignItems: 'center', backgroundColor: '#4C144C', alignSelf: 'center', height: 40, justifyContent: 'center', borderRadius: 5 , marginRight: 10},
    ButtonText: {marginLeft: 20, marginRight: 20, fontSize: 12, color: '#fff'}
})