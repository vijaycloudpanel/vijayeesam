import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    headerStyle: { backgroundColor: "white", height: 55, elevation: 5 },
    headerText: { fontSize: 16,  color: "#000", width: 200, },
    OfflineText: {alignSelf: 'center', marginTop: '40%' },
    ButtonStyle: { alignItems: 'center', backgroundColor: '#4C144C', alignSelf: 'center', height: 40, justifyContent: 'center', borderRadius: 5, width: '75%', marginTop: 40, marginBottom: 20 },
    ButtonText: { marginLeft: 20, marginRight: 20, fontSize: 12, color: '#fff' },
    SubText_1: { height: '100%', fontSize: 14, marginLeft: 10, width: '74%' },
    SubView_1: {height: 45,flexDirection: 'row', borderWidth: 0.3, alignItems: 'center', marginTop: 20, width: '75%', alignSelf: 'center'},
    SubImg_1: { width: 30, color: '#000', marginLeft: 10 },

})