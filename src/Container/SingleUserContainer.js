import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    headerStyle: { backgroundColor: "white", height: 55, elevation: 5 },
    headerText: { fontSize: 16, color: "#000", width: 200, },
    OfflineText: {alignSelf: 'center', marginTop: '40%' },
    ImageStyle: { height: 150, width: 150, borderRadius: 75, alignSelf: 'center', marginTop: 20,marginBottom: 10 },
    cardView: { margin: 10, backgroundColor: '#fff', borderWidth: 0.5, borderColor: '#f5f5f5', elevation: 0.5, borderRadius: 5 },
    UserText: {fontSize: 16, fontWeight: 'bold', alignSelf: 'center'},
    mainView: {flexDirection: 'row', justifyContent: 'space-between', marginTop: 5},
    SubView1: {width: '35%'},
    SubView2: {width: '65%'},
    HardText: {marginTop: 10,marginLeft: 10, fontSize: 14, fontWeight: 'bold'},
    SoftText: {marginTop: 10, fontSize: 14},
    SoftText1: {marginTop: 10, fontSize: 14, marginBottom: 10}
})