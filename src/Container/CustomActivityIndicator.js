import React, { Component } from "react";
import { View } from "react-native";
import  Constants  from "../Common/Constants";
import { BallIndicator } from "react-native-indicators";

class CustomActivityIndicator extends Component {
  render() {
    return (
      <View style={Constants.ActivityIndicatorView}>
        <BallIndicator color={'#C82257'} size={25} />
      </View>
    );
  }
}

export default CustomActivityIndicator;