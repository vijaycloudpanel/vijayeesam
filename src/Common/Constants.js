const Constants = {

  OffLineMsg: "You are offline. Check your internet.",

  StartPointURL: "https://reqres.in/",
  CreateURL: "api/users",
  ListUserURL: "api/users?page=2",
  SingleUserURL: "api/users/",
  methodGet: "GET",
  methodPost: "POST",

  ActivityIndicatorView: {
    flex: 1,
    padding: '50%',
    alignSelf: 'center',
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "white"
  },

  TOAST: {
    LENGTH_LONG: 5000,
    LENGTH_SHORT: 2000,
    LENGTH_MEDIUM: 3000,
    OPACITY: 0.8,
    POSITION_BOTTOM: "bottom",
    POSITION_TOP: "top",
    FADE_IN_DURATION: 750,
    FADE_OUT_DURATION: 1000
  },
}

export default Constants;