import { SINGLEUSER_FETCH, SINGLE_LOADER } from '../Common/ConstantKey';
import SingleUserComponent from '../Component/SingleUserComponent';
import { connect } from 'react-redux';
import Constants from '../Common/Constants';

export const DEFAULT_STATE_SINGLEUSER = {
    isLoading: true,
    isData: [],
}

export const SingleUserFetchedData = (isData) => {
    return {
        type: SINGLEUSER_FETCH,
        isData
    }
}

export const SingleUser_FetchData = (id) => {
    var request_Headers = {
        method: Constants.methodGet, headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
        },
    };
    var request_UserURL = (Constants.StartPointURL + Constants.SingleUserURL + id);
    return (dispatch) => {
        return fetch(request_UserURL, request_Headers)
            .then((res) => res.json())
            .then((responseJson) => {
                if (responseJson != undefined && responseJson != null && responseJson != "") {
                    return dispatch(SingleUserFetchedData(responseJson));
                }
            }).catch((error) => {
                console.log(error);
                // var Data = "";
                // return dispatch(ServerError(Data));
            })
    }
}

const mapStateToProps = (state) => {
    return {
        SingleUserData: state.SingleUserReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderSingleUserFetch: (id) => {
            return dispatch(SingleUser_FetchData(id));
        },
        onRenderLoader: () =>{
            return dispatch({type: SINGLE_LOADER});
        } 
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SingleUserComponent);
