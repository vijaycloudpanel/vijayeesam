import { HOME_FETCH } from '../Common/ConstantKey';
import HomeComponent from '../Component/HomeComponent';
import { connect } from 'react-redux';
import Constants from '../Common/Constants';

export const DEFAULT_STATE_HOME = {
    isData: [],
}

export const HomeFetchedData = (isData) => {
    return {
        type: HOME_FETCH,
        isData
    }
}

export const Home_FetchData = (name, job) => {
    var request_Headers = {
        method: Constants.methodPost, headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
        },
        body: JSON.stringify({
            "name": name,
            "job": job
        })

    };
    var request_UserURL = (Constants.StartPointURL + Constants.CreateURL);
    return (dispatch) => {
        return fetch(request_UserURL, request_Headers)
            .then((res) => res.json())
            .then((responseJson) => {
                if (responseJson != undefined && responseJson != null && responseJson != "") {
                    return dispatch(HomeFetchedData(responseJson));
                } 
            }).catch((error) => {
                console.log(error);
                // var Data = "";
                // return dispatch(ServerError(Data));
            })
    }
}

const mapStateToProps = (state) => {
    return {
        HomeData: state.HomeReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderHomeFetch: (name , job) => {
           return dispatch(Home_FetchData(name , job));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);
