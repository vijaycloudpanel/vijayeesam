import { LISTUSER_FETCH } from '../Common/ConstantKey';
import ListUserComponent from '../Component/ListUserComponent';
import { connect } from 'react-redux';
import Constants from '../Common/Constants';

export const DEFAULT_STATE_LISTUSER = {
    isLoading: true,
    isData: [],
}

export const ListUserFetchedData = (isData) => {
    return {
        type: LISTUSER_FETCH,
        isData
    }
}

export const ListUser_FetchData = () => {
    var request_Headers = {
        method: Constants.methodGet, headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
        },
    };
    var request_UserURL = (Constants.StartPointURL + Constants.ListUserURL);
    return (dispatch) => {
        return fetch(request_UserURL, request_Headers)
            .then((res) => res.json())
            .then((responseJson) => {
                if (responseJson != undefined && responseJson != null && responseJson != "") {
                    return dispatch(ListUserFetchedData(responseJson));
                }
            }).catch((error) => {
                console.log(error);
                // var Data = "";
                // return dispatch(ServerError(Data));
            })
    }
}

const mapStateToProps = (state) => {
    return {
        ListUserData: state.ListUserReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderListUserFetch: () => {
            return dispatch(ListUser_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListUserComponent);
