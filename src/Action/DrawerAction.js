import { DRAWER_FETCH } from '../Common/ConstantKey';
import DrawerComponent from '../Component/DrawerComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_DRAWER = {
    LOGOUTFETCHDATA : []
}

export const LOGOUT_FETCHDATA = (LOGOUTFETCHDATA) => {
    return {
        type : DRAWER_FETCH,
        LOGOUTFETCHDATA
    }
}

const mapStateToProps = (state) => {
    return {
        DrawerData: state.DrawerReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
           return dispatch(LOGOUT_FETCHDATA())
       }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent);
