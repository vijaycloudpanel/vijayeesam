import React, { Component } from 'react';
import { Text, View, StatusBar, TouchableOpacity, ScrollView, Image, BackHandler } from 'react-native';
import { Container, Left, Header, Right } from 'native-base';
import SingleUserContainer from '../Container/SingleUserContainer';
import CustomActivityIndicator from '../Container/CustomActivityIndicator';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import Constants from '../Common/Constants';
import NetInfo from '@react-native-community/netinfo';
import Toast from 'react-native-easy-toast';

export default class SingleUserComponent extends Component {

    componentDidMount() {
        console.error = (error) => error.apply;
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            this.retrieveItem();
        })
        console.disableYellowBox = true;
    }

        UNSAFE_componentWillMount() {

            NetInfo.addEventListener(this.handleConnectionChange);
    
            NetInfo.fetch().then(state => {
                this.setState({ status: state.isConnected });
            });
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    
        }
    
        componentWillUnmount() {
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    
        handleBackButton() {
            this.back();
            return true;
        }
    
        handleConnectionChange = (state) => {
            this.setState({ status: state.isConnected });
        }


back(){
    this.props.onRenderLoader();
 this.props.navigation.goBack()
}

    retrieveItem() {
        this.props.onRenderSingleUserFetch(this.state.UserId);
    }

    constructor(props) {
        super(props);
        const UserId = this.props.navigation.state.params.UserId
        this.state = {
            status: true,
            UserId: UserId
        }
        this.back = this.back.bind(this);
        this.handleBackButton = this.handleBackButton.bind(this);
    }


    render() {
        return (
            <Container style={{ backgroundColor: "#fff" }}>
                <Toast ref="toast"
                    style={{ backgroundColor: '#000' }}
                    position={Constants.TOAST.POSITION_BOTTOM}
                    positionValue={200}
                    fadeInDuration={Constants.TOAST.FADE_IN_DURATION}
                    fadeOutDuration={Constants.TOAST.FADE_OUT_DURATION}
                    opacity={Constants.TOAST.OPACITY}
                    textStyle={{ color: '#fff' }}
                />
                <Header style={SingleUserContainer.headerStyle}>
                    <Left>
                        <TouchableOpacity underlayColor="transparent" onPress={this.back}>
                            <Icon
                                name="arrow-left"
                                color="#000"
                                size={22}
                            /></TouchableOpacity>
                    </Left>
                    <TouchableOpacity
                        activeOpacity={0.5} underlayColor="#d3d3d3" underlayColor="transparent" style={{ marginLeft: -45, width: 100 , justifyContent: 'center'}}>
                        <Text style={SingleUserContainer.headerText}>Single User</Text>
                    </TouchableOpacity>
                    <Right>
                    </Right>
                </Header>
                <StatusBar hidden={false} />
                <ScrollView>

                    {(!this.state.status) ? (<Text style = {SingleUserContainer.OfflineText}>You're in Offline</Text> ) :
                     (this.props.SingleUserData.isLoading && this.state.status) ? (<CustomActivityIndicator />) : ( this.props.SingleUserData.isData != undefined && this.props.SingleUserData.isData != '') ? (
                            
                        <View>
                        <Image
                            source={{ uri: this.props.SingleUserData.isData.data.avatar }}
                            style={SingleUserContainer.ImageStyle}
                        />
                     <View style={SingleUserContainer.cardView}>
                     <Text style = {SingleUserContainer.UserText}>User Details</Text>
                       <View style = {SingleUserContainer.mainView}>
<View style = {SingleUserContainer.SubView1}>
                         <Text style = {SingleUserContainer.HardText}>First Name</Text>
                         <Text style = {SingleUserContainer.HardText}>Last Name</Text>
                         <Text style = {SingleUserContainer.HardText}>E-mail ID</Text>
                         <Text style = {SingleUserContainer.HardText}>Customer ID</Text>
                         <Text style = {SingleUserContainer.HardText}>Company</Text>
                         <Text style = {SingleUserContainer.HardText}>Quotes</Text>
</View>
<View style ={SingleUserContainer.SubView2}>
                         <Text style = {SingleUserContainer.SoftText}>{this.props.SingleUserData.isData.data.first_name }</Text>
                         <Text style = {SingleUserContainer.SoftText}>{this.props.SingleUserData.isData.data.last_name }</Text>
                         <Text style = {SingleUserContainer.SoftText}>{this.props.SingleUserData.isData.data.email }</Text>
                         <Text style = {SingleUserContainer.SoftText}>{this.props.SingleUserData.isData.data.id }</Text>
                         <Text style = {SingleUserContainer.SoftText}>{this.props.SingleUserData.isData.ad.company }</Text>
                         <Text style = {SingleUserContainer.SoftText1}>{this.props.SingleUserData.isData.ad.text }</Text>
</View>
                       </View>
                      
                         </View>   
</View>
                       
                    
                        ):(<CustomActivityIndicator />)}
                </ScrollView>
            </Container>
        )
    }
}