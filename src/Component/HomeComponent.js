import React, { Component } from 'react';
import { Text, View, StatusBar, TouchableOpacity, TextInput, ScrollView, Keyboard } from 'react-native';
import { Container, Left, Header, Right } from 'native-base';
import HomeContainer from '../Container/HomeContainer';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import Constants from '../Common/Constants';
import NetInfo from '@react-native-community/netinfo';
import Toast from 'react-native-easy-toast';

export default class HomeComponent extends Component {

    componentDidMount() {
        console.error = (error) => error.apply;
        console.disableYellowBox = true;
    }


    UNSAFE_componentWillMount() {

        NetInfo.addEventListener(this.handleConnectionChange);

        NetInfo.fetch().then(state => {
            this.setState({ status: state.isConnected });
        });
    }

    componentWillUnmount() {
        //NetInfo.removeEventListener(this.handleConnectionChange);
    }

    handleConnectionChange = (state) => {
        this.setState({ status: state.isConnected });
    }

    constructor() {
        super();
        this.state = {
            status: true,
            name: '',
            job: '',
        }
        this.onjobEditHandle = (job) => this.setState({ job });
        this.onnameEditHandle = (name) => this.setState({ name });
        this.CallApi = this.CallApi.bind(this);

    }

    CallApi(){
        Keyboard.dismiss();
        if(this.state.status){
            if(this.state.name== ''){
                this.refs.toast.show("Name value is required", Constants.TOAST.LENGTH_SHORT)
            }else if(this.state.job == ''){
                this.refs.toast.show("Job is required", Constants.TOAST.LENGTH_SHORT)
            }else{
        this.props.onRenderHomeFetch(this.state.name, this.state.job).then((res) =>{
            if(this.props.HomeData.isData != undefined && this.props.HomeData.isData != ''){
        this.refs.toast.show("User created successfully.", Constants.TOAST.LENGTH_SHORT);
        this.setState({name: '', job: ''})
    }
        })
    }
        }else{
            this.refs.toast.show(Constants.OffLineMsg, Constants.TOAST.LENGTH_SHORT)
        }
    }

    render() {
        return (
            <Container>
                   <Toast ref="toast"
                    style={{ backgroundColor: '#000' }}
                    position={Constants.TOAST.POSITION_BOTTOM}
                    positionValue={200}
                    fadeInDuration={Constants.TOAST.FADE_IN_DURATION}
                    fadeOutDuration={Constants.TOAST.FADE_OUT_DURATION}
                    opacity={Constants.TOAST.OPACITY}
                    textStyle={{ color: '#fff' }}
                />
                   <Header style={HomeContainer.headerStyle}>
                    <Left>
                        <TouchableOpacity underlayColor="transparent" onPress = {()=>this.props.navigation.openDrawer() }>
                            <Icon
                                name="menu"
                                color="#000"
                                size={20}
                            /></TouchableOpacity>
                    </Left>
                    <TouchableOpacity
                        activeOpacity={0.5} underlayColor="#d3d3d3" underlayColor="transparent" style={{ marginLeft: -45, width: 100 , justifyContent: 'center'}}>
                        <Text style={HomeContainer.headerText}>Create User</Text>
                    </TouchableOpacity>
                    <Right>
                    </Right>
                </Header>
                <StatusBar hidden={false} />
                <ScrollView>
                    {(!this.state.status) ? (<Text style = {HomeContainer.OfflineText}>You're in Offline</Text> ) : (
                    <View style = {{marginTop: '20%'}}>
                    <View style={HomeContainer.SubView_1}>
                        <TextInput
                            style={HomeContainer.SubText_1}
                            autoCapitalize={'none'}
                            autoFocus={true}
                            editable={true}
                            value={this.state.name}
                            blurOnSubmit={false}
                            onChangeText={this.onnameEditHandle}
                            returnKeyType={'next'}
                            placeholder='Name'
                        />
                    </View>

                    <View style={HomeContainer.SubView_1}>
                
                        <TextInput
                            style={HomeContainer.SubText_1}
                            autoCapitalize={'none'}
                            editable={true}
                            value={this.state.job}
                            blurOnSubmit={false}
                            onChangeText={this.onjobEditHandle}
                            returnKeyType={'done'}
                            placeholder='Job'
                        />
                    </View>

                    <TouchableOpacity onPress = {this.CallApi}
                        style={HomeContainer.ButtonStyle}>
                        <Text style={HomeContainer.ButtonText}>CREATE</Text>
                    </TouchableOpacity>
                    </View>)}
                </ScrollView>
            </Container>
        )
    }
}