import React, { Component } from 'react';
import { Text, View, StatusBar, TouchableOpacity, SafeAreaView, BackHandler } from 'react-native';
import { Container } from 'native-base';
import DrawerContainer from '../Container/DrawerContainer';

export default class DrawerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
   
    }

    componentDidMount() {
        console.error = error => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }
   

    render() {
        StatusBar.setBackgroundColor("#F0F2F3");

        return (
            <Container style={DrawerContainer.containerStyle}>
                <SafeAreaView>

                    <View style={{ marginTop: 20 }}>

                        <TouchableOpacity activeOpacity={0.5}
                            style={{ height: 40 }}
                            onPress={() => this.props.navigation.navigate("Home")}>
                            <Text style={DrawerContainer.leftTextStyle}>Create User</Text>

                        </TouchableOpacity>
                    </View>


                    <View style={{ top: '10%' }}>

                        <TouchableOpacity activeOpacity={0.5}
                            onPress={() => this.props.navigation.navigate("ListUser")}>
                            <Text style={DrawerContainer.leftTextStyle}>List Of User</Text>

                        </TouchableOpacity>

                    </View>
                </SafeAreaView>
            </Container>
        );
    }
}