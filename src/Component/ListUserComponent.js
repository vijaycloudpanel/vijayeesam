import React, { Component } from 'react';
import { Text, View, StatusBar, TouchableOpacity, FlatList, ScrollView, Image } from 'react-native';
import { Container, Left, Header, Right } from 'native-base';
import ListUserContainer from '../Container/ListUserContainer';
import CustomActivityIndicator from '../Container/CustomActivityIndicator';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import Constants from '../Common/Constants';
import NetInfo from '@react-native-community/netinfo';
import Toast from 'react-native-easy-toast';

export default class ListUserComponent extends Component {

    componentDidMount() {
        console.error = (error) => error.apply;
        this.retrieveItem();
        console.disableYellowBox = true;
    }


    UNSAFE_componentWillMount() {

        NetInfo.addEventListener(this.handleConnectionChange);

        NetInfo.fetch().then(state => {
            this.setState({ status: state.isConnected });
        });
    }

    componentWillUnmount() {
        //NetInfo.removeEventListener(this.handleConnectionChange);
    }

    handleConnectionChange = (state) => {
        this.setState({ status: state.isConnected });
    }

    retrieveItem() {
        this.props.onRenderListUserFetch();
    }

    constructor() {
        super();
        this.state = {
            status: true,
        }

    }



    render() {
        return (
            <Container style={{ backgroundColor: "#f5f5f5" }}>
                <Toast ref="toast"
                    style={{ backgroundColor: '#000' }}
                    position={Constants.TOAST.POSITION_BOTTOM}
                    positionValue={200}
                    fadeInDuration={Constants.TOAST.FADE_IN_DURATION}
                    fadeOutDuration={Constants.TOAST.FADE_OUT_DURATION}
                    opacity={Constants.TOAST.OPACITY}
                    textStyle={{ color: '#fff' }}
                />
                <Header style={ListUserContainer.headerStyle}>
                    <Left>
                        <TouchableOpacity underlayColor="transparent" onPress = {()=>this.props.navigation.openDrawer()}>
                            <Icon
                                name="menu"
                                color="#000"
                                size={20}
                            /></TouchableOpacity>
                    </Left>
                    <TouchableOpacity
                        activeOpacity={0.5} underlayColor="#d3d3d3" underlayColor="transparent" style={{ marginLeft: -45, width: 100 , justifyContent: 'center'}}>
                        <Text style={ListUserContainer.headerText}>List Of User</Text>
                    </TouchableOpacity>
                    <Right>
                    </Right>
                </Header>
                <StatusBar hidden={false} />
                <ScrollView>

                    {(!this.state.status) ? (<Text style = {ListUserContainer.offLineText}>You're in Offline</Text> ) :
                     (this.props.ListUserData.isLoading && this.state.status) ? (<CustomActivityIndicator />) : ( this.props.ListUserData.isData != undefined && this.props.ListUserData.isData != '') ? (
                        <FlatList
                        style={{ marginBottom: 20 }}
                        data={this.props.ListUserData.isData.data}
                        keyExtractor={(item, index) => index}
                        renderItem={({ item }) =>
                        <View style={ListUserContainer.cardView}>
                        <Image
                            source={{ uri: item.avatar }}
                            style={ListUserContainer.ImageStyle}
                        />
                        <Text style={{ fontSize: 12 }}>{item.first_name}{" "}{item.last_name}</Text>
                        <TouchableOpacity style={ListUserContainer.ButtonStyle} onPress={() => this.props.navigation.navigate("SingleUser", { UserId: item.id })}>
                            <Text style ={ListUserContainer.ButtonText}>{'View'}</Text>
                        </TouchableOpacity>
                    </View>
                     }
                     />
                        ):(<Text style = {ListUserContainer.offLineText}>Data is Empty</Text>)}
                </ScrollView>
            </Container>
        )
    }
}