import { SINGLEUSER_FETCH,SINGLE_LOADER } from '../Common/ConstantKey';
import { DEFAULT_STATE_SINGLEUSER } from '../Action/SingleUserAction';

const SingleUserReducer = (state = DEFAULT_STATE_SINGLEUSER, action) => {

    switch (action.type) {
        case SINGLEUSER_FETCH:
            return {
                ...state,
                isData: action.isData,
                isLoading: false
            };

            case SINGLE_LOADER:
                return {
                    ...state,
                    isData: []
                };
        default:
            return state;
    }

}

export default SingleUserReducer;