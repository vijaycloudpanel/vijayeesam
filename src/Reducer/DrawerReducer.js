import { DRAWER_FETCH } from "../Common/ConstantKey";
import { DEFAULT_STATE_DRAWER } from "../Action/DrawerAction";

const DrawerReducer = (state = DEFAULT_STATE_DRAWER, action) => {
    switch (action.type) {
        case DRAWER_FETCH:
            return {
                ...state,
                LOGOUTFETCHDATA: action.LOGOUTFETCHDATA,
            };
        default:
            return state;
    }
}
export default DrawerReducer;