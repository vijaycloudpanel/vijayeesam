import { LISTUSER_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_LISTUSER } from '../Action/ListUserAction';

const ListUserReducer = (state = DEFAULT_STATE_LISTUSER, action) => {

    switch (action.type) {
        case LISTUSER_FETCH:
            return {
                ...state,
                isData: action.isData,
                isLoading: false
            };

        default:
            return state;
    }

}

export default ListUserReducer;