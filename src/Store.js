import { combineReducers, createStore } from 'redux'
import HomeReducer from './Reducer/HomeReducer';
import ListUserReducer from './Reducer/ListUserReducer';
import SingleUserReducer from './Reducer/SingleUserReducer';
import DrawerReducer from './Reducer/DrawerReducer';

const AppReducers = combineReducers({
    HomeReducer, ListUserReducer, SingleUserReducer, DrawerReducer
})
// let store = createStore(rootReducer);

export default AppReducers;