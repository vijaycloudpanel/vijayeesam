import React, { Component } from "react";
import { Provider } from "react-redux";
import HomeAction from './src/Action/HomeAction';
import ListUserAction from './src/Action/ListUserAction';
import SingleUserAction from './src/Action/SingleUserAction';
import DrawerAction from './src/Action/DrawerAction';
import configureStore from "./configureStore";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

const store = configureStore();

const Drawer = createDrawerNavigator({
  Home: { screen: HomeAction },
  ListUser: { screen: ListUserAction},
 
},
  {
    initialRouteName: "Home",
    contentComponent: DrawerAction,
    drawerPosition: "Left",
    drawerOpenRoute: "DrawerOpen",
    drawerCloseRoute: "DrawerClose",
    drawerToggleRoute: "DrawerToggle",
    drawerWidth: 250

  })

  const Stack = createStackNavigator(
    {
      Home: { screen: Drawer },
      ListUser: { screen: ListUserAction},
      SingleUser: { screen: SingleUserAction },

    },
  
    {
      headerMode: "none"
    }
  );

  const AppContainer = createAppContainer(Stack);


class App extends Component {

  render() {
    // console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

export default App;