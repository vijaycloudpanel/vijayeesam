import {createStore, applyMiddleware} from 'redux';
import AppReducers from './src/Store';
import thunk from'redux-thunk';

export default function configureStore(){
    let store = createStore(AppReducers,
        {},
    applyMiddleware(thunk)
    );
    return store
}